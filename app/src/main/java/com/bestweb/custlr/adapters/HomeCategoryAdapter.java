package com.bestweb.custlr.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.RecyclerClickListener;
import com.bestweb.custlr.interfaces.SubCategoryClickListener;
import com.bestweb.custlr.interfaces.WishListListener;
import com.bestweb.custlr.models.Categories;
import com.bestweb.custlr.models.Subcategory;

import java.util.ArrayList;

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.MyViewHolder> {
    private ArrayList<Categories> arrayList;
    private Context context;
    private RecyclerClickListener recyclerClickListener;
    private WishListListener wishListListener;
    private SubCategoryClickListener subCategoryClickListener;

    public HomeCategoryAdapter(Context context, ArrayList<Categories> arrayList, RecyclerClickListener recyclerClickListener, WishListListener wishListListener, SubCategoryClickListener subCategoryClickListener) {
        this.context = context;
        this.arrayList = arrayList;
        this.recyclerClickListener = recyclerClickListener;
        this.wishListListener = wishListListener;
        this.subCategoryClickListener = subCategoryClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_products_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        String title = arrayList.get(position).getCategoryName();
        ArrayList<Subcategory> subcategoryArrayList = arrayList.get(position).getSubCategoryList();


        if (subcategoryArrayList.size() > 0) {
            holder.categoryTxt.setText(title);
        } else {
            holder.relativeText.setVisibility(View.GONE);
        }


        HomeSubCatAdapter homeSubCatAdapter = new HomeSubCatAdapter(context, subcategoryArrayList, wishListListener, subCategoryClickListener);
        holder.subCatRecycler.setAdapter(homeSubCatAdapter);

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView categoryTxt;
        public RecyclerView subCatRecycler;
        public RelativeLayout relativeText;


        public MyViewHolder(View itemView) {
            super(itemView);

            categoryTxt = itemView.findViewById(R.id.hpi_categoryTxt);
            subCatRecycler = itemView.findViewById(R.id.hpi_subCatRecycler);
            relativeText = itemView.findViewById(R.id.hp_relativeTitle);

            subCatRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));


            relativeText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerClickListener.onItemClicked(getAdapterPosition());
                }
            });

        }
    }
}