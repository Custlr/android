package com.bestweb.custlr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.models.MainCategories;

import java.util.ArrayList;

public class PrdPopUpAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<MainCategories> arrayList = new ArrayList<>();

    public PrdPopUpAdapter(Context context, ArrayList<MainCategories> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        if (arrayList == null || arrayList.size() == 0) {
            return 0;
        }
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_pop_local_kind, null);
            holder.textView = convertView.findViewById(R.id.item_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textView.setText(arrayList.get(i).getMainCatName());
        return convertView;
    }

    private class ViewHolder {
        TextView textView;
    }
}
