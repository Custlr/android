package com.bestweb.custlr.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.WishListClickListener;
import com.bestweb.custlr.models.Products;
import com.bestweb.custlr.utils.MyUtils;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Products> productsArrayList = new ArrayList<>();
    private WishListClickListener wishListClickListener = null;

    public WishListAdapter(Context context, ArrayList<Products> productsArrayList, WishListClickListener wishListClickListener) {
        this.context = context;
        this.productsArrayList = productsArrayList;
        this.wishListClickListener = wishListClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_layout_wishlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String productImageUrl = productsArrayList.get(position).getAppThumbnail();
        String productName = productsArrayList.get(position).getName();
        String price = productsArrayList.get(position).getPrice();

        Glide.with(context).load(productImageUrl).into(holder.productImageView);

        holder.productNameTxt.setText(MyUtils.checkStringValue(productName) ? productName : "-");
        holder.productPriceTxt.setText(MyUtils.checkStringValue(price) ? ("RM" + price) : "-");

    }

    @Override
    public int getItemCount() {
        return productsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productNameTxt, productPriceTxt, addToCartTxt, deleteTxt;
        private PorterShapeImageView productImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            this.productImageView = itemView.findViewById(R.id.ilw_productImageView);

            this.productNameTxt = itemView.findViewById(R.id.ilw_productNameTxt);
            this.productPriceTxt = itemView.findViewById(R.id.ilw_productPriceTxt);
            this.addToCartTxt = itemView.findViewById(R.id.ilw_addToCartTxt);
            this.deleteTxt = itemView.findViewById(R.id.ilw_deleteTxt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wishListClickListener.onWishListItemClicked(getAdapterPosition());
                }
            });

            addToCartTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wishListClickListener.onWishListItemClicked(getAdapterPosition());
                }
            });

            deleteTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String productId = "" + productsArrayList.get(getAdapterPosition()).getId();
                    wishListClickListener.onDeleteWishList(getAdapterPosition(), productId);
                }
            });
        }
    }
}