package com.bestweb.custlr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.bestweb.custlr.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class BannerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> arrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public BannerAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.banner_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.bi_imageView);

        Glide.with(context).load(arrayList.get(position)).into(imageView);

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}