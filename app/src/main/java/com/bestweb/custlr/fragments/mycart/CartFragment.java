package com.bestweb.custlr.fragments.mycart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.adapters.MyCartListAdapter;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.SampleJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CartFragment extends Fragment {

    private RecyclerView recyclerView;
    private TextView txtHeader;
    private LinearLayout linearNext, linearBack;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mycart, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        listener();


    }

    private void init() {
        recyclerView = getActivity().findViewById(R.id.fm_recycler);

        linearBack = getActivity().findViewById(R.id.am_linearBack);
        linearNext = getActivity().findViewById(R.id.am_linearNext);

        txtHeader = getActivity().findViewById(R.id.cart_txtHeader);
        txtHeader.setText("MY CART");

    }

    private void listener() {


        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        linearNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUtils.ReplaceFragmentOpen(getActivity(), new Delivery());

            }
        });


        loadImages();


    }


    private void loadImages() {
      /*  try {
            JSONObject jsonObject = new JSONObject(SampleJson.SEARCH_PRODUCTS_JSON);
            JSONArray jsonArray = jsonObject.optJSONArray("banner_images");

            ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                String imageUrl = jsonArray.optJSONObject(i).optString("img");
                arrayList.add(imageUrl);
            }

            MyCartListAdapter adapter = new MyCartListAdapter(arrayList, getActivity());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

    }


}
