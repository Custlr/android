package com.bestweb.custlr.fragments.mycart;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.HomeActivity;
import com.bestweb.custlr.utils.MyUtils;

public class Order extends Fragment {

    // private RecyclerView recyclerView;
    private TextView txtHeader;
    private LinearLayout linearNext, linearBack;
    private RadioButton radioSeanang, radioPaypal, radioBank;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        listener();


    }

    private void init() {
        //recyclerView = getActivity().findViewById(R.id.fm_recycler);

        linearBack = getActivity().findViewById(R.id.fo_linearBack);
        linearNext = getActivity().findViewById(R.id.fo_linearNext);


        radioSeanang = getActivity().findViewById(R.id.radioSenang);
        radioPaypal = getActivity().findViewById(R.id.radioPaypal);
        radioBank = getActivity().findViewById(R.id.radioBank);

        txtHeader = getActivity().findViewById(R.id.cart_txtHeader);
        txtHeader.setText("ORDER");

    }

    private void listener() {


        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUtils.ReplaceFragmentClose(getActivity(), new Payment());
            }
        });

        linearNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HomeActivity.class));
                Toast.makeText(getActivity(), "Your order have completed successfully", Toast.LENGTH_SHORT).show();

            }
        });

        radioSeanang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkRadio(R.id.radioSenang);
            }
        });

        radioPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkRadio(R.id.radioPaypal);
            }
        });

        radioBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkRadio(R.id.radioBank);

            }
        });


    }

    private void checkRadio(int id) {


        switch (id) {
            case R.id.radioSenang:
                radioSeanang.setChecked(true);
                radioPaypal.setChecked(false);
                radioBank.setChecked(false);
                Log.e("ok", "radioSenang");
                break;

            case R.id.radioPaypal:
                radioPaypal.setChecked(true);
                radioSeanang.setChecked(false);
                radioBank.setChecked(false);
                Log.e("ok", "radioPayPal");
                break;

            case R.id.radioBank:
                radioBank.setChecked(true);
                radioPaypal.setChecked(false);
                radioSeanang.setChecked(false);
                Log.e("ok", "radioBank");
                break;

        }

        /*if(id==R.id.radioSenang){
            radioSeanang.setChecked(true);
            radioPaypal.setChecked(false);
            radioBank.setChecked(false);
            Log.e("ok","radioSenang");
        }else if(id==R.id.radioPaypal){
            radioPaypal.setChecked(true);
            radioSeanang.setChecked(false);
            radioBank.setChecked(false);
            Log.e("ok","radioPaypal");

        }else if(id==R.id.radioBank) {
            radioBank.setChecked(true);
            radioPaypal.setChecked(false);
            radioSeanang.setChecked(false);
            Log.e("ok","radioBank");
        }*/

    }


}
