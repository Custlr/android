package com.bestweb.custlr.fragments.mycart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bestweb.custlr.R;
import com.bestweb.custlr.utils.MyUtils;

public class Delivery extends Fragment {

    private TextView txtHeader;
    private LinearLayout linearNext, linearBack;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_delivery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        listener();
    }

    private void init() {

        linearBack = getActivity().findViewById(R.id.fd_linearBack);
        linearNext = getActivity().findViewById(R.id.fd_linearNext);

        txtHeader = getActivity().findViewById(R.id.cart_txtHeader);
        txtHeader.setText("DELIVERY");
    }

    private void listener() {


        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUtils.ReplaceFragmentClose(getActivity(), new CartFragment());
            }
        });

        linearNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUtils.ReplaceFragmentOpen(getActivity(), new Payment());

            }
        });


    }

}
