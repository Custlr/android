package com.bestweb.custlr.utils;

import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.content.ContextCompat;

public class PermissionRequest {


    public static boolean askForPermission(String permission, int requestCode/*, Activity activity,*/, Activity activity) {

        if (isAboveLollipop()) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
               /* if (fragment.shouldShowRequestPermissionRationale(permission)) {
                    fragment.requestPermissions(new String[]{permission}, requestCode);
                    return false;
                } else {
                    fragment.requestPermissions(new String[]{permission}, requestCode);
                    return false;
                }*/

                activity.requestPermissions(new String[]{permission}, requestCode);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }

    }

    /*public static boolean askForPermission(String permission, Integer requestCode, Activity activity) {

        if(isAboveLollipop()){

            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
                    return true;
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
                    return false;
                }
            } else {
                return true;

            }
        }else {
            return true;
        }

    }*/

    public static boolean isAboveLollipop() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP;
    }
}
