package com.bestweb.custlr.utils;


import com.ciyashop.library.apicall.URLS;

public class APIS {


// ==========================================================================

    public static final String BASE_URL = "https://custlr.com.my/";
    //public static final String BASE_URL = "https://demo.bestweb.com.my/custlr/";
    public static final String EXTENSION = "wp-json/pgs-woo-api/v1/";
    public final String WOO_MAIN_URL = BASE_URL + "wp-json/wc/v2/";
    public final String MAIN_URL = BASE_URL + EXTENSION;
    public static final String DEVICE_TYPE = "2";


    public static final String CONSUMERKEY = "RoN2iwFyrVwl";
    public static final String CONSUMERSECRET = "xk6pAlVFLYeYqMrFPYt2H3atCFmRP0FhPz3Ec9mnjKKMqwwi";
    public static final String OAUTH_TOKEN = "SUC0mvPCYGZzhH0cuuQYVASy";
    public static final String OAUTH_TOKEN_SECRET = "f4reB5ykN9k7ZnQSSB8kgHshUGtSKksoUAHkXl7fgURSNTls";

    public static final String WOOCONSUMERKEY = "ck_97f0bbf115dcb85f19f953327ce67c138556e602";
    public static final String WOOCONSUMERSECRET = "cs_3c4405aed694c91a103354dcaf5e049f8640ca43";
    public static final String version = "3.1.6";

    /*public static final String CONSUMERKEY = "IT3ZhCn6aA14";
    public static final String CONSUMERSECRET = "tk4k9tYHODHVhTwyhlfP9QeAbAGuO6aLtgzCvczM3jVRqnXY";
    public static final String OAUTH_TOKEN = "nlWH62I3D6OLf14mJYuTTQ22";
    public static final String OAUTH_TOKEN_SECRET = "qcMMuh6TOKwKW0h9JbzCNA3RU7VU0umIeiCzBdQgRyEsc07b";

    public static final String WOOCONSUMERKEY = "ck_97f0bbf115dcb85f19f953327ce67c138556e602";
    public static final String WOOCONSUMERSECRET = "cs_3c4405aed694c91a103354dcaf5e049f8640ca43";
    public static final String version = "3.1.6";*/

    /*public static final String CONSUMERKEY = "8ibNxlzcCXCE";
    public static final String CONSUMERSECRET = "dvLjbY8TfFdqlnB7xANZGsFkqUwErmxe4ZFzxf1Ys86a0hYL";
    public static final String OAUTH_TOKEN = "anC455VwnAtr2p8nDX1ZYLur";
    public static final String OAUTH_TOKEN_SECRET = "gNFiQMi2E94Gn6wD8hmJl5EdJHInwmOk8Nf0KoNGMIjyXvoz";

    public static final String WOOCONSUMERKEY = "ck_da560b1a5bb633c0e8b42a0fdfb59baf0c998f39";
    public static final String WOOCONSUMERSECRET = "cs_3f607d136bb73e18a7103a8ace36c9b626611bdf";
    public static final String version = "3.1.6";*/

    //================================================================================

    public APIS() {
        URLS.APP_URL = BASE_URL;
        URLS.WOO_MAIN_URL = WOO_MAIN_URL;
        URLS.MAIN_URL = MAIN_URL;
        URLS.version = version;
        URLS.CONSUMERKEY = CONSUMERKEY;
        URLS.CONSUMERSECRET = CONSUMERSECRET;
        URLS.OAUTH_TOKEN = OAUTH_TOKEN;
        URLS.OAUTH_TOKEN_SECRET = OAUTH_TOKEN_SECRET;
        URLS.WOOCONSUMERKEY = WOOCONSUMERKEY;
        URLS.WOOCONSUMERSECRET = WOOCONSUMERSECRET;
    }


    public static final String LOGIN_API = EXTENSION + "login";
    public static final String HOME_API = EXTENSION + "home";
    public static final String REGSITER_API = EXTENSION + "create_customer";
    public static final String FORGOT_PASSWORD = EXTENSION + "forgot_password";
    public static final String UPDATE_PASSWORD = EXTENSION + "update_password";
    public static final String PRODUCTS_LIST = EXTENSION + "products";
    public static final String PRODUCTS_DETAILS = EXTENSION + "products";
    public static final String GET_CUSTOMER = EXTENSION + "customer";
    public static final String UPDATE_USER_IMAGE = EXTENSION + "update_user_image";
    public static final String UPDATE_CUSTOMER = EXTENSION + "update_customer";
    public static final String SEARCH = EXTENSION + "live_search";
    public static final String ADD_WISHLIST = EXTENSION + "add_wishlist";
    public static final String REMOVE_WISHLIST = EXTENSION + "remove_wishlist";
    public static final String ADD_REVIEW = EXTENSION + "postreview";
    public static final String MY_ORDERS = EXTENSION + "orders";
    public static final String CHECKOUT_PRODUCT = EXTENSION + "add_to_cart";
    public static final String WISH_LIST_IDS = EXTENSION + "wishlist";
    public static final String LOGOUT_PAYMENT = EXTENSION + "logout";

    public static final String EXTENSION_NEW = "api/bestweb/";
    public static final String GET_MEASUREMENT_VALUES = EXTENSION_NEW + "product_details";
    public static final String ADD_TO_CART = EXTENSION_NEW + "add_cart";
    public static final String GET_MEASUREMENTS_PROFILE = EXTENSION_NEW + "get_body_mesurments";
    public static final String UPDATE_MEASUREMENT = EXTENSION_NEW + "body_mesurments_update";
    public static final String GET_CART = EXTENSION_NEW + "get_cart";
    public static final String DELETE_CART_ITEM = EXTENSION_NEW + "delete_cart";
    public static final String GET_CART_AND_WISH_LIST_COUNT = EXTENSION_NEW + "get_count";


}
