package com.bestweb.custlr.interfaces;

import com.bestweb.custlr.utils.custom_view.spark_button.SparkButton;

public interface WishListListener {

    void onWishListListen(SparkButton wishList, String productId);

}
