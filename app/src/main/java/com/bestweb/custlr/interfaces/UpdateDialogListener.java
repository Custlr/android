package com.bestweb.custlr.interfaces;

public interface UpdateDialogListener {
    void onUpdateButtonClicked();

    void onNotNowButtonClicked();

    void onDismissButtonClicked();

}
