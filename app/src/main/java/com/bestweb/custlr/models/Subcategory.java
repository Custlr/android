package com.bestweb.custlr.models;

public class Subcategory {

    String productId;
    String product_url;
    String product_name;
    String regularPrice;
    String salePrice;
    boolean isWishList;

    public void setWishList(boolean wishList) {
        isWishList = wishList;
    }

    public Subcategory(String productId, String product_url, String product_name, String regularPrice, String salePrice) {
        this.productId = productId;
        this.product_url = product_url;
        this.product_name = product_name;
        this.regularPrice = regularPrice;
        this.salePrice = salePrice;
    }

    public String getProductId() {
        return productId;
    }

    public String getProduct_url() {
        return product_url;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getRegularPrice() {
        return regularPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public boolean isWishList() {
        return isWishList;
    }
}
