package com.bestweb.custlr.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.ConnectivityInterface;
import com.bestweb.custlr.interfaces.DialogInterface;
import com.bestweb.custlr.interfaces.UpdateDialogListener;
import com.bestweb.custlr.utils.AppUpdateDialog;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.CustomDialog;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.NetworkChangeReceiver;
import com.bestweb.custlr.utils.PreferenceHandler;
import com.bestweb.custlr.utils.PreferenceManager;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

public class SplashScreen extends AppCompatActivity {

    private Context context;
    private PreferenceManager prefManager = null;
    private BroadcastReceiver mNetworkReceiver = null;
    private CustomDialog customInternetDialog = null;

    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    private AppUpdateDialog appUpdateDialog = null;
    private CustomDialog configErrorDialog = null;

    private String userId = "";
    private String fcmToken = "";
    private String deviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        context = SplashScreen.this;
        prefManager = new PreferenceManager(context);

        ImageView imageView = findViewById(R.id.as_splashScreenImage);
        Glide.with(context).load(R.drawable.launch_screen).into(imageView);
    }


    private void intentToWelcomePage() {
        Intent intent = new Intent(context, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void intentToHomePage() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        finish();
    }



    

    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    //=======Internet listener starts here=======================================================================


    private void startNetworkBroadcast() {
        if (mNetworkReceiver != null) {

            Log.e("register", "not null");

            unregisterNetworkChanges();

            registerNetworkBroadcast();

        } else {
            Log.e("register", "null");
            registerNetworkBroadcast();
        }
    }

    private void registerNetworkBroadcast() {
        mNetworkReceiver = new NetworkChangeReceiver(new ConnectivityInterface() {
            @Override
            public void onConnected() {

                dismissIfDialogsAreOpned();

                getRemoteConfigValues();

            }

            @Override
            public void onNotConnected() {
                Log.e("network", "not-connected");
                showNoInternetAlertDialog();
            }
        });
        registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    protected void unregisterNetworkChanges() {
        if (mNetworkReceiver != null) {
            unregisterReceiver(mNetworkReceiver);
        }
    }

    private CustomDialog getNoInternetAlertDialog() {
        customInternetDialog = new CustomDialog(SplashScreen.this, Constants.DIALOG_WARNING, "Please check your data connection", new DialogInterface() {
            @Override
            public void onButtonClicked() {
                Intent settingsIntent = new Intent(Settings.ACTION_SETTINGS);
                startActivity(settingsIntent);
            }

            @Override
            public void onDismissedClicked() {
                finish();
            }
        });
        customInternetDialog.setTitleMessage("No Internet Connection!");
        customInternetDialog.showDialog();
        return customInternetDialog;
    }

    private void showNoInternetAlertDialog() {

        dismissIfDialogsAreOpned();

        customInternetDialog = getNoInternetAlertDialog();
    }


    private void dismissIfDialogsAreOpned() {
        if (customInternetDialog != null) {
            customInternetDialog.dismissDialog();
        }

        if (appUpdateDialog != null) {
            appUpdateDialog.dismissDialog();
        }
        if (configErrorDialog != null) {
            configErrorDialog.dismissDialog();
        }
    }


    //==================get remote config values and compare app update======================================

    private void getRemoteConfigValues() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(0)
                .setFetchTimeoutInSeconds(30)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        mFirebaseRemoteConfig.fetchAndActivate().addOnCompleteListener(SplashScreen.this, new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                try {
                    if (task.isSuccessful()) {

                        Log.e("config", "sts:" + "loaded");

                        String appVersionName = mFirebaseRemoteConfig.getString(Constants.APP_VERSION_NAME);
                        double appVersionCode = mFirebaseRemoteConfig.getDouble(Constants.APP_VERSION_CODE);
                        boolean isForceUpdate = mFirebaseRemoteConfig.getBoolean(Constants.APP_IS_FORCE_UPDATE);
                        String appUpdateContent = mFirebaseRemoteConfig.getString(Constants.APP_UPDATE_CONTENT);

                        Log.e("versionCode", ":" + appVersionCode);

                        if (appVersionCode > MyUtils.getVersionCode()) {

                            showUpdateDialog(appVersionName, isForceUpdate, appUpdateContent);

                        } else {
                            getFcmToken();
                        }


                    } else {
                        showConfigErrorDialog();
                        Log.e("config", "sts:" + "failed");
                        Log.e("config", "sts11:" + ""+task.getException());
                    }
                } catch (Exception e) {
                    Log.e("config", "exp:" + e.getMessage());
                    showConfigErrorDialog();
                }
            }
        });
    }

    private void showUpdateDialog(String appVersionName, boolean isForceUpdate, String appUpdateContent) {
        appUpdateDialog = new AppUpdateDialog(SplashScreen.this, appUpdateContent, new UpdateDialogListener() {
            @Override
            public void onUpdateButtonClicked() {
                redirectToPlayStore();
            }

            @Override
            public void onNotNowButtonClicked() {
                getFcmToken();
            }

            @Override
            public void onDismissButtonClicked() {
                finish();
            }
        });
        appUpdateDialog.setTitleMessage("App Update Available V-" + appVersionName);
        appUpdateDialog.showNotNowButton(isForceUpdate);
        appUpdateDialog.showDialog();
    }


//=================get notification token and proceeds further to activities=============================================


    private void getFcmToken() {

        deviceId = MyUtils.getDeviceId(context);
        Log.e("deviceId", "deviceId: " + deviceId);

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w("notifications", "getInstanceId failed", task.getException());
                }
                fcmToken = task.getResult().getToken();

                PreferenceHandler.storePreference(context, Constants.FCM_TOKEN, fcmToken);
                Log.e("fcm_token", "token: " + fcmToken);

                if (prefManager!=null && prefManager.isFirstTimeLaunch()) {
                    intentToHomePage();
                } else {
                    intentToWelcomePage();
                }

            }
        });

    }


    //==========re-directions to other app or activities=========================================================
    private void redirectToPlayStore() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + MyUtils.getAppPackageName(context))));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + MyUtils.getAppPackageName(context))));
        }
    }


    private void showConfigErrorDialog() {
        configErrorDialog = new CustomDialog(SplashScreen.this, Constants.DIALOG_WARNING, Constants.ERROR_REMOTE_CONFIG, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                finish();
            }

            @Override
            public void onDismissedClicked() {
                finish();
            }
        });
        configErrorDialog.setTitleMessage("Oops..Something went wrong");
        configErrorDialog.showDialog();
    }



    //============on-resume && on-destroy====================
    @Override
    protected void onResume() {
        super.onResume();
        startNetworkBroadcast();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }



}
