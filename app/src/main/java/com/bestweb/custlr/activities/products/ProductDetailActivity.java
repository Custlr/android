package com.bestweb.custlr.activities.products;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.LoginActivity;
import com.bestweb.custlr.activities.products.measurements.ProductMeasurements;
import com.bestweb.custlr.adapters.ProductDetailTabAdapter;
import com.bestweb.custlr.adapters.ProductImageAdapter;
import com.bestweb.custlr.models.ProductDetails;
import com.bestweb.custlr.models.WishList;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.utils.custom_view.spark_button.SparkButton;
import com.bestweb.custlr.utils.custom_view.spark_button.SparkEventListener;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AppCompatActivity {


    private Context context;
    private ImageView imgNext, imgBack, shareImage;
    private ProgressBar progressBar;
    private TabLayout descriptionTabLayout;
    private ViewPager productImagePager, descriptionPager;
    private LinearLayout dotsLinear;
    private TextView txtHeader, nameTxt, salePriceTxt, regularPriceTxt;
    private SparkButton wishListImage;
    private LinearLayout linearLayoutMeasure;
    private ProgressDialog progressDialog = null;

    private String productId;
    private String productName;
    private String productRegularPrice;
    private String productSalePrice;
    private String isFromPage = "";
    private ArrayList<ProductDetails> productArrayList = new ArrayList<>();
    ArrayList<String> wishListIdsArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        context = ProductDetailActivity.this;

        imgBack = findViewById(R.id.imgBack);
        txtHeader = findViewById(R.id.txtHeader);

        productImagePager = findViewById(R.id.ap_productsPager);
        dotsLinear = findViewById(R.id.ap_dots);
        progressBar = findViewById(R.id.ap_progressBar);
        nameTxt = findViewById(R.id.ap_nameTxt);
        salePriceTxt = findViewById(R.id.ap_salePriceTxt);
        regularPriceTxt = findViewById(R.id.ap_regularPriceTxt);

        shareImage = findViewById(R.id.ap_shareImage);
        wishListImage = findViewById(R.id.ap_wishListImage);

        linearLayoutMeasure = findViewById(R.id.ap_measureLinear);

        descriptionTabLayout = findViewById(R.id.ap_tab_layout);
        descriptionPager = findViewById(R.id.ap_view_pager);

        imgNext = findViewById(R.id.ap_nextImg);

        txtHeader.setText("DETAILS");

        Bundle bundle = getIntent().getExtras();
        productId = bundle.getString(Constants.PRODUCT_ID);
        productName = bundle.getString(Constants.PRODUCT_NAME);
        productRegularPrice = bundle.getString(Constants.PRODUCT_REGULAR_PRICE);
        productSalePrice = bundle.getString(Constants.PRODUCT_SALE_PRICE);
        isFromPage = bundle.getString(Constants.IS_FROM);

        setUpProductName(productName);

        setUpProductPrice(productSalePrice, productRegularPrice);


        if (MyUtils.checkStringValue(isFromPage) && isFromPage.equalsIgnoreCase(Constants.IS_FROM_CART_PAGE)) {
            linearLayoutMeasure.setAlpha((float) 0.5);
            imgNext.setAlpha((float) 0.5);

            getProductDetail(true);
        } else {
            getProductDetail(false);
        }


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePage();
            }
        });


    }

    private void getProductDetail(final boolean isFromCartPage) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getProductsDetails(JsonInput.getProductDetails(productId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        if (response != null) {
                            updateUI(response.body(), isFromCartPage);
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void updateUI(String jsonResponse, final boolean isFromCartPage) {
        Log.e("prd_detail", ":" + jsonResponse);

        productArrayList.clear();
        Type listType = new TypeToken<ArrayList<ProductDetails>>() {
        }.getType();
        Gson gson = new GsonBuilder().serializeNulls().create();
        productArrayList = gson.fromJson(jsonResponse, listType);

        if (productArrayList.size() > 0) {

            String salePrice = productArrayList.get(0).getSalePrice();
            String regularPrice = productArrayList.get(0).getRegularPrice();

            setUpProductPrice(salePrice, regularPrice);

            ArrayList<ProductDetails.Image> imageArrayList = productArrayList.get(0).getImages();
            loadProductImages(imageArrayList);

            String description = productArrayList.get(0).getDescription();
            String productId = "" + productArrayList.get(0).getId();
            String productName = "" + productArrayList.get(0).getName();
            String productPrice = "" + productArrayList.get(0).getPrice();
            String productImageUrl = "" + productArrayList.get(0).getAppThumbnail();
            final String permaLink = "" + productArrayList.get(0).getPermalink();


            final Bundle bundle = new Bundle();
            bundle.putString(Constants.PRODUCT_ID, productId);
            bundle.putString(Constants.PRODUCT_NAME, productName);
            bundle.putString(Constants.PRODUCT_PRICE, productPrice);
            bundle.putString(Constants.PRODUCT_IMAGE_URL, productImageUrl);

            setUpDescriptionTabLayout(description, permaLink, bundle);


            setUpWishListImageIcon();


            imgNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isFromCartPage) {
                        intentToProductsMeasurement(bundle);
                    }
                }
            });

            shareImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (MyUtils.checkStringValue(permaLink)) {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_TEXT, permaLink);
                        startActivity(Intent.createChooser(shareIntent, "Share link using"));
                    }

                }
            });


            linearLayoutMeasure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isFromCartPage) {
                        intentToProductsMeasurement(bundle);
                    }
                }
            });


        }
    }

    private void setUpProductName(String prdName) {
        nameTxt.setText(prdName);
    }

    private void setUpProductPrice(String salePrice, String regularPrice) {
        if (MyUtils.checkStringValue(salePrice)) {

            salePriceTxt.setText("RM " + salePrice);

            if (MyUtils.checkStringValue(regularPrice)) {
                regularPriceTxt.setVisibility(View.VISIBLE);
                regularPriceTxt.setText("RM " + regularPrice);
                strikeRegularPrice(regularPriceTxt);
            } else {
                regularPriceTxt.setVisibility(View.GONE);
            }

        } else {
            salePriceTxt.setText(MyUtils.checkStringValue(regularPrice) ? ("RM " + regularPrice) : "");
            regularPriceTxt.setVisibility(View.GONE);
        }
    }


    private void strikeRegularPrice(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }


    private void loadProductImages(final ArrayList<ProductDetails.Image> imageArrayList) {
        try {
            ProductImageAdapter bannerAdapter = new ProductImageAdapter(context, imageArrayList);
            productImagePager.setAdapter(bannerAdapter);
            addBottomDots(imageArrayList.size(), 0);
            productImagePager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    addBottomDots(imageArrayList.size(), position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addBottomDots(int size, int currentPage) {
        if (size > 0) {
            TextView[] dots = new TextView[size];

            dotsLinear.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(this);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(40);
                dots[i].setTextColor(Color.parseColor("#D4D8DB"));
                dotsLinear.addView(dots[i]);
            }

            if (dots.length > 0) {
                dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
                dots[currentPage].setTextSize(45);
            }
        }
    }


    private void setUpWishListImageIcon() {
        String userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {

            getWishListIds(productId, userId);

        } else {
            setUpWishListImage(false);
        }
    }


    private void getWishListIds(final String productId, String userId) {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getWishListIds(JsonInput.getWishListIds(userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        String jsonResponse = response.body();

                        Log.e("ids_response", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase("success")) {

                                if (jsonObject.has("sync_list")) {
                                    JSONArray jsonArray = jsonObject.optJSONArray("sync_list");

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        wishListIdsArrayList.clear();
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            String prod_id = jsonArray.optJSONObject(i).optString("prod_id");
                                            wishListIdsArrayList.add(prod_id);
                                        }

                                        setUpWishListImage(checkIfProductsInWishList(productId));

                                    } else {
                                        setUpWishListImage(false);
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e.getMessage());
        }
    }

    private boolean checkIfProductsInWishList(String productId) {
        return wishListIdsArrayList.size() > 0 && wishListIdsArrayList.contains(productId);
    }


    private void setUpWishListImage(boolean state) {
        wishListImage.setVisibility(View.VISIBLE);
        wishListImage.setChecked(state);
        wishListImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (wishListImage.isChecked()) {
                    wishListImage.setChecked(false);
                    wishListImage.playAnimation();
                } else {
                    wishListImage.setChecked(true);
                    wishListImage.playAnimation();
                }

            }
        });

        wishListImage.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                Log.e("event", ":" + "onEvent");
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {
                Log.e("event", ":" + "onEventAnimationEnd");

                checkWishListStatus(wishListImage, productId);

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {
                Log.e("event", ":" + "onEventAnimationStart");
            }
        });

    }


    //============= wishList add/remove =========================================

    private void checkWishListStatus(SparkButton wishListButton, String productId) {
        String userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {

            addProductToWishList(wishListButton.isChecked(), wishListButton, productId, userId);

        } else {
            disableWishListButton(wishListButton);

            intentToLoginPage();
        }


        if (wishListButton.isChecked()) {
            Log.e("wishlist", "addProduct:" + productId);
        } else {
            Log.e("wishlist", "removeProduct:" + productId);
        }
    }


    private void addProductToWishList(boolean status, final SparkButton wishListButton, String productId, String userId) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, status ? "Adding to WishList" : "Removing from WishList");
            Call<String> call = getWishListCall(status, productId, userId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {

                            Type type = new TypeToken<WishList>() {
                            }.getType();
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            WishList wishList = gson.fromJson(response.body(), type);

                            if (wishList != null) {

                                String status = wishList.getStatus();

                                if (MyUtils.checkStringValue(status)) {
                                    if (status.equalsIgnoreCase(Constants.SUCCESS)) {

                                        String message = wishList.getMessage();

                                        MyUtils.ShowLongToast(context, "" + message);

                                    } else if (status.equalsIgnoreCase(Constants.ERROR)) {

                                        String error = wishList.getMessage();

                                        if (MyUtils.checkStringValue(error)) {

                                            MyUtils.ShowLongToast(context, "" + error);

                                            if (!error.equalsIgnoreCase("exists")) {
                                                disableWishListButton(wishListButton);
                                            }
                                        } else {
                                            disableWishListButton(wishListButton);
                                        }

                                    }
                                } else {
                                    disableWishListButton(wishListButton);
                                }
                            }


                            Log.e("wishlist", "resp:" + response.body());
                        }
                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                        disableWishListButton(wishListButton);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    disableWishListButton(wishListButton);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            disableWishListButton(wishListButton);
        }
    }

    private Call getWishListCall(boolean status, String productId, String userId) {
        ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
        if (status) {
            return apiService.addToWishList(JsonInput.wishListInput(userId, productId));
        } else {
            return apiService.removeFromWishList(JsonInput.wishListInput(userId, productId));
        }
    }

    private void disableWishListButton(SparkButton wishListButton) {
        if (wishListButton.isChecked()) {
            wishListButton.setChecked(false);
        }
    }

    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, ProductDetailActivity.this);
    }

    private void intentToProductsMeasurement(Bundle bundle) {
        Intent intent = new Intent(context, ProductMeasurements.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, ProductDetailActivity.this);
    }


    private void setUpDescriptionTabLayout(String description, String permaLink, Bundle bundle) {
        descriptionTabLayout.addTab(descriptionTabLayout.newTab().setText("Description"));
        descriptionTabLayout.addTab(descriptionTabLayout.newTab().setText("Reviews"));

        ProductDetailTabAdapter tabsAdapter = new ProductDetailTabAdapter(getSupportFragmentManager(), descriptionTabLayout.getTabCount(), description, permaLink, bundle);
        descriptionPager.setAdapter(tabsAdapter);
        descriptionPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(descriptionTabLayout));
        descriptionTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                descriptionPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.LOGIN_CODE) {
            if (resultCode == Constants.LOGIN_SUCCESS) {
                setUpWishListImageIcon();
            }
        }
    }


    private void closePage() {
        setResult(Constants.PRODUCTS);
        finish();
        MyUtils.openOverrideAnimation(false, ProductDetailActivity.this);
    }

    @Override
    public void onBackPressed() {
        closePage();
    }
}
