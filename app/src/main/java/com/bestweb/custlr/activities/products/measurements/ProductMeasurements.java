package com.bestweb.custlr.activities.products.measurements;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bestweb.custlr.R;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.PreferenceHandler;
import com.bestweb.custlr.utils.custom_view.ShimmerLayout;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductMeasurements extends AppCompatActivity {

    private Context context;
    private TextView txtHeader;
    private ImageView imgBack;
    private ShimmerLayout shimmerLayout;
    private LinearLayout linearBodyMeasure, linearA4Fit, linearStandardSize;
    //private ProgressDialog progressDialog = null;

    private String featuredImageUrl = null;
    private JSONArray styleArray = new JSONArray();
    private JSONArray measurementValueArray = new JSONArray();
    private JSONArray selectArray = new JSONArray();
    private JSONArray uploadArray = new JSONArray();

    String productId = "";
    String productName = "";
    String productPrice = "";
    String imgUrl = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_measurements);
        context = ProductMeasurements.this;

        init();
        listener();
        getMeasurementValues();

    }

    private void init() {
        txtHeader = findViewById(R.id.txtHeader);
        imgBack = findViewById(R.id.imgBack);
        shimmerLayout = findViewById(R.id.adm_shimmer_layout);
        linearBodyMeasure = findViewById(R.id.adm_linearBodyMeasure);
        linearA4Fit = findViewById(R.id.adm_linearA4Fit);
        linearStandardSize = findViewById(R.id.adm_linearStandardSize);

        txtHeader.setText("LETS FIT YOU");

        Bundle bundle = getIntent().getExtras();
        productId = bundle.getString(Constants.PRODUCT_ID);
        productName = bundle.getString(Constants.PRODUCT_NAME);
        productPrice = bundle.getString(Constants.PRODUCT_PRICE);
        imgUrl = bundle.getString(Constants.PRODUCT_IMAGE_URL);

    }

    private void listener() {

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                MyUtils.openOverrideAnimation(false, ProductMeasurements.this);
            }
        });


    }


    private void getMeasurementValues() {
        try {
            showShimmerLayout();
            //progressDialog = MyUtils.showProgressLoader(context, "Loading...");
            ApiInterface apiService = HttpRequest.getScalarsInstanceWC().create(ApiInterface.class);
            Call<String> call = apiService.getMeasurementValues(productId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    stopShimmerAnimation();
                    //MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Log.e("response", "" + response.body());
                            parseValues(response.body());
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    stopShimmerAnimation();
                    //MyUtils.dismissProgressLoader(progressDialog);

                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e);
            stopShimmerAnimation();
            //MyUtils.dismissProgressLoader(progressDialog);
        }
    }

    private void parseValues(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject dataObject = jsonObject.optJSONObject("data");

            featuredImageUrl = dataObject.optString("featured_image");
            styleArray = dataObject.optJSONArray("style");
            measurementValueArray = dataObject.optJSONArray("mesurments_name");
            selectArray = dataObject.optJSONArray("Select");
            uploadArray = dataObject.optJSONArray("upload");


            linearBodyMeasure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.PRODUCT_ID, productId);
                    bundle.putString(Constants.PRODUCT_NAME, productName);
                    bundle.putString(Constants.PRODUCT_PRICE, productPrice);
                    bundle.putString(Constants.PRODUCT_IMAGE_URL, imgUrl);
                    bundle.putString(Constants.SIZE, "");
                    bundle.putString(Constants.FEATURED_IMAGE_URL, featuredImageUrl);
                    bundle.putString(Constants.STYLE_JSON_ARRAY, styleArray != null ? styleArray.toString() : "");
                    bundle.putString(Constants.MEASUREMENT_VALUES, measurementValueArray != null ? measurementValueArray.toString() : "");

                    PreferenceHandler.storePreference(context, "postType", Constants.BODY_MEASURE_POST);


                    Intent intent = new Intent(ProductMeasurements.this, BodyMeasurement.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    MyUtils.openOverrideAnimation(true, ProductMeasurements.this);

                }
            });

            linearA4Fit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.PRODUCT_ID, productId);
                    bundle.putString(Constants.PRODUCT_NAME, productName);
                    bundle.putString(Constants.PRODUCT_PRICE, productPrice);
                    bundle.putString(Constants.PRODUCT_IMAGE_URL, imgUrl);
                    bundle.putString(Constants.STYLE_JSON_ARRAY, styleArray != null ? styleArray.toString() : "");
                    bundle.putString(Constants.MEASUREMENT_VALUES, measurementValueArray != null ? measurementValueArray.toString() : "");
                    bundle.putString(Constants.UPLOAD, uploadArray != null ? uploadArray.toString() : "");

                    PreferenceHandler.storePreference(context, "postType", Constants.A4FIT_MEASURE_POST);

                    Intent intent = new Intent(ProductMeasurements.this, A4Fit_Measurement.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    MyUtils.openOverrideAnimation(true, ProductMeasurements.this);
                }
            });

            linearStandardSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.PRODUCT_ID, productId);
                    bundle.putString(Constants.PRODUCT_NAME, productName);
                    bundle.putString(Constants.PRODUCT_PRICE, productPrice);
                    bundle.putString(Constants.PRODUCT_IMAGE_URL, imgUrl);
                    bundle.putString(Constants.FEATURED_IMAGE_URL, featuredImageUrl);
                    bundle.putString(Constants.STYLE_JSON_ARRAY, styleArray != null ? styleArray.toString() : "");
                    bundle.putString(Constants.MEASUREMENT_VALUES, measurementValueArray != null ? measurementValueArray.toString() : "");
                    bundle.putString(Constants.SELECT, selectArray != null ? selectArray.toString() : "");

                    PreferenceHandler.storePreference(context, "postType", Constants.STANDARD_SIZE_POST);

                    Intent intent = new Intent(ProductMeasurements.this, StandardSize.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    MyUtils.openOverrideAnimation(true, ProductMeasurements.this);

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showShimmerLayout() {
        shimmerLayout.setVisibility(View.VISIBLE);
        shimmerLayout.startShimmerAnimation();
    }

    private void stopShimmerAnimation() {
        if (shimmerLayout.getVisibility() == View.VISIBLE) {
            shimmerLayout.setVisibility(View.GONE);
        }
        if (shimmerLayout.isAnimationStarted()) {
            shimmerLayout.stopShimmerAnimation();
        }
    }
}
