package com.bestweb.custlr.activities.products.measurements;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bestweb.custlr.R;
import com.bestweb.custlr.fragments.bottom_sheet_dialog.BottomSheetExpendable;
import com.bestweb.custlr.interfaces.UploadAlertListener;
import com.bestweb.custlr.models.MeasurementValuesModel;
import com.bestweb.custlr.models.StyleModel;
import com.bestweb.custlr.models.UploadFileModel;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.PermissionAlert;
import com.bestweb.custlr.utils.PermissionRequest;
import com.bestweb.custlr.utils.RealPathUtil;
import com.bestweb.custlr.utils.UploadAlert;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class A4Fit_Measurement extends AppCompatActivity {

    private ImageView imgBack, frontImg, backImg, imgCut;
    private TextView txtHeader;
    private ViewPager viewPager;
    private LinearLayout dotsLayout, linearSummary;
    private MyViewPagerAdapter myViewPagerAdapter;
    private EditText edtWeight, edtHeight, edtRemarks;

    private TextView[] dots;
    private int[] layouts;

    ArrayList<MeasurementValuesModel> measurementValueArrayList = new ArrayList<>();
    ArrayList<UploadFileModel> uploadFileModelArrayList = new ArrayList<>();
    ArrayList<StyleModel> styleModelArrayList = new ArrayList<>();
    HashMap<String, String> map = new HashMap<>();

    String styleJsonArray = "";

    String filePathFront = "";
    String filePathSide = "";

    private String imageFilePath = null;
    private boolean isImageLoaded = false;
    private boolean isFrontImage = false;

    String productId = "", productName = "", productPrice = "", imgUrl = "";
    String measurementValues = null;
    String uploadValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a4_fit__measurement);

        init();
        listener();
    }

    private void init() {

        imgBack = findViewById(R.id.imgBack);
        imgCut = findViewById(R.id.a4_linearCut);
        txtHeader = findViewById(R.id.txtHeader);
        dotsLayout = findViewById(R.id.a4_dots);

        edtWeight = findViewById(R.id.a4_edtWeight);
        edtHeight = findViewById(R.id.a4_edtHeight);

        viewPager = findViewById(R.id.a4_view_pager);
        frontImg = findViewById(R.id.a4_frontImg);
        backImg = findViewById(R.id.a4_backImg);
        edtRemarks = findViewById(R.id.a4_edtRemarks);


        linearSummary = findViewById(R.id.a4_linearSummaryButton);

        txtHeader.setText("A4 FIT MEASUREMENTS");

        layouts = new int[]{R.layout.front_image_screen, R.layout.side_image_screen};

        // adding bottom dots
        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);

        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);


        Bundle bundle = getIntent().getExtras();
        productId = bundle.getString(Constants.PRODUCT_ID);
        productName = bundle.getString(Constants.PRODUCT_NAME);
        productPrice = bundle.getString(Constants.PRODUCT_PRICE);
        imgUrl = bundle.getString(Constants.PRODUCT_IMAGE_URL);
        measurementValues = bundle.getString(Constants.MEASUREMENT_VALUES);
        styleJsonArray = bundle.getString(Constants.STYLE_JSON_ARRAY);
        uploadValue = bundle.getString(Constants.UPLOAD);


    }

    private void listener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                MyUtils.openOverrideAnimation(false, A4Fit_Measurement.this);
            }
        });


        frontImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFrontImage = true;
                showUploadAlert();
            }
        });

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFrontImage = false;
                showUploadAlert();
            }
        });

        imgCut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (styleModelArrayList.size() > 0) {
                    BottomSheetExpendable bottomSheetExpendable = new BottomSheetExpendable(styleModelArrayList);
                    bottomSheetExpendable.show(getSupportFragmentManager(), "tag");
                }
            }
        });

        linearSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String weight = edtWeight.getText().toString();
                String height = edtHeight.getText().toString();

                if (!MyUtils.checkStringValue(imageFilePath)) {
                    MyUtils.ShowLongToast(A4Fit_Measurement.this, "Please Choose Image File");
                } else if (!MyUtils.checkStringValue(weight)) {
                    MyUtils.ShowLongToast(A4Fit_Measurement.this, "Please Enter Weight size");
                } else if (!MyUtils.checkStringValue(edtRemarks.getText().toString())) {
                    MyUtils.ShowLongToast(A4Fit_Measurement.this, "Please Enter Remarks");
                } else if (MyUtils.checkStringValue(height)) {
                    putValuesToHashMap();
                    intentToSummaryActivity(weight, height, filePathFront, filePathSide, map);
                } else {
                    MyUtils.ShowLongToast(A4Fit_Measurement.this, "Please Enter Height size");
                }

            }
        });


        if (MyUtils.checkStringValue(measurementValues)) {
            Type listType = new TypeToken<ArrayList<MeasurementValuesModel>>() {
            }.getType();
            Gson gson = new GsonBuilder().serializeNulls().create();
            measurementValueArrayList = gson.fromJson(measurementValues, listType);
        }

        if (MyUtils.checkStringValue(uploadValue)) {
            Type listType = new TypeToken<ArrayList<UploadFileModel>>() {
            }.getType();
            Gson gson = new GsonBuilder().serializeNulls().create();
            uploadFileModelArrayList = gson.fromJson(uploadValue, listType);
        }

        if (MyUtils.checkStringValue(styleJsonArray)) {
            Type listType = new TypeToken<ArrayList<StyleModel>>() {
            }.getType();
            Gson gson = new GsonBuilder().serializeNulls().create();
            styleModelArrayList = gson.fromJson(styleJsonArray, listType);

            setAllTheDefaultChildSelected();
        }

    }


    private void setAllTheDefaultChildSelected() {
        if (styleModelArrayList.size() > 0) {
            for (int i = 0; i < styleModelArrayList.size(); i++) {
                if (styleModelArrayList.get(i).getValue().size() > 0) {
                    styleModelArrayList.get(i).getValue().get(0).setSelected(true);
                }
            }
        }
    }

    private void putValuesToHashMap() {

        if (styleModelArrayList.size() > 0) {

            for (int i = 0; i < styleModelArrayList.size(); i++) {

                String groupName = styleModelArrayList.get(i).getName();
                String section = styleModelArrayList.get(i).getSection();
                ArrayList<StyleModel.Value> childArrayList = styleModelArrayList.get(i).getValue();

                for (int k = 0; k < childArrayList.size(); k++) {
                    boolean isSelected = childArrayList.get(k).isSelected();
                    if (isSelected) {
                        String styleName = childArrayList.get(k).getDetails();
                        String styleType = childArrayList.get(k).getType();
                        String image = childArrayList.get(k).getImage();

                        if (groupName.equalsIgnoreCase("Collar")) {

                            map.put("collar_name", groupName);
                            map.put("collar_type", styleType);
                            map.put("collar_value", styleName);
                            map.put("collar_section", section);
                            map.put("collar_key", styleName + "_1");
                            map.put("collar_image", image);
                        }

                        if (groupName.equalsIgnoreCase("Cuff")) {

                            map.put("cuff_name", groupName);
                            map.put("cuff_type", styleType);
                            map.put("cuff_value", styleName);
                            map.put("cuff_section", section);
                            map.put("cuff_key", styleName + "_1");
                            map.put("cuff_image", image);
                        }

                        if (groupName.equalsIgnoreCase("Sleeve")) {

                            map.put("sleeve_name", groupName);
                            map.put("sleeve_type", styleType);
                            map.put("sleeve_value", styleName);
                            map.put("sleeve_section", section);
                            map.put("sleeve_key", styleName + "_1");
                            map.put("sleeve_image", image);
                        }

                        if (groupName.equalsIgnoreCase("Fitting")) {

                            map.put("fitting_name", groupName);
                            map.put("fitting_type", styleType);
                            map.put("fitting_value", styleName);
                            map.put("fitting_section", section);
                            map.put("fitting_key", styleName + "_1");
                            map.put("fitting_image", image);
                        }

                        if (groupName.equalsIgnoreCase("Chest Pocket")) {

                            map.put("chest_pocket_name", groupName);
                            map.put("chest_pocket_type", styleType);
                            map.put("chest_pocket_value", styleName);
                            map.put("chest_pocket_section", section);
                            map.put("chest_pocket_key", styleName + "_1");
                            map.put("chest_pocket_image", image);
                        }


                    }
                }
            }

        }

        if (measurementValueArrayList.size() > 0) {

            for (int i = 0; i < measurementValueArrayList.size(); i++) {

                String css_class = "";
                css_class = measurementValueArrayList.get(i).getCssclass();
                String section = "";
                section = measurementValueArrayList.get(i).getSection();
                String name = "";
                name = measurementValueArrayList.get(i).getName();

                if (name.equalsIgnoreCase("Weight")) {
                    map.put("weight_class", css_class);
                    map.put("weight_name", name);
                    map.put("weight_section", section);
                }

                if (name.equalsIgnoreCase("Height")) {
                    map.put("height_class", css_class);
                    map.put("height_name", name);
                    map.put("height_section", section);
                }

                if (name.equalsIgnoreCase("Remark")) {
                    map.put("remark_class", css_class);
                    map.put("remark_name", name);
                    map.put("remark_section", section);
                }


            }
        }

        if (uploadFileModelArrayList.size() > 0) {

            for (int i = 0; i < uploadFileModelArrayList.size(); i++) {

                String css_class = "";
                css_class = uploadFileModelArrayList.get(i).getCssclass();
                String section = "";
                section = uploadFileModelArrayList.get(i).getSection();
                String name = "";
                name = uploadFileModelArrayList.get(i).getName();

                if (name.equalsIgnoreCase("Front Image")) {
                    map.put("front_image_class", css_class);
                    map.put("front_image_name", name);
                    map.put("front_image_section", section);
                }

                if (name.equalsIgnoreCase("Side Image")) {
                    map.put("side_image_class", css_class);
                    map.put("side_image_name", name);
                    map.put("side_image_section", section);
                }


            }
        }


    }

    private void intentToSummaryActivity(String weight, String height, String filePathFront, String filePathSide, HashMap<String, String> hashMap) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.IS_FROM, Constants.IS_FROM_A4FIT);
        bundle.putString(Constants.PRODUCT_ID, productId);
        bundle.putString(Constants.PRODUCT_NAME, productName);
        bundle.putString(Constants.PRODUCT_PRICE, productPrice);
        bundle.putString(Constants.PRODUCT_IMAGE_URL, imgUrl);
        bundle.putString(Constants.REMARKS, edtRemarks.getText().toString());

        bundle.putString(Constants.WEIGHT, weight);
        bundle.putString(Constants.HEIGHT, height);

        bundle.putString(Constants.IMAGE_FRONT, filePathFront);
        bundle.putString(Constants.IMAGE_SIDE, filePathSide);

        bundle.putSerializable(Constants.HEIGHT_WEIGHT, hashMap);

        Intent intent = new Intent(A4Fit_Measurement.this, SummaryActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

        MyUtils.openOverrideAnimation(true, A4Fit_Measurement.this);

    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(45);
            dots[i].setTextColor(Color.parseColor("#D4D8DB"));
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#000000"));
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


    private void showUploadAlert() {
        UploadAlert uploadAlert = new UploadAlert(A4Fit_Measurement.this, new UploadAlertListener() {
            @Override
            public void onChooseFileClicked() {
                if (PermissionRequest.askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_FILES_PERMISSION, A4Fit_Measurement.this)) {
                    intentToImageSelection();
                }
            }

            @Override
            public void onCaptureCameraClicked() {
                if (PermissionRequest.askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_CAMERA_PERMISSION, A4Fit_Measurement.this)) {
                    intentToCameraApp();
                }
            }
        });
        uploadAlert.showAlertDialog();
    }

    private void intentToImageSelection() {
        isImageLoaded = false;
        imageFilePath = null;
        MyUtils.intentToImageSelection(A4Fit_Measurement.this);
    }

    private void intentToCameraApp() {
        isImageLoaded = false;
        imageFilePath = null;
        imageFilePath = MyUtils.intentToCameraApp(A4Fit_Measurement.this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == Constants.PICK_IMAGE_FROM_FILES) {

                    imageFilePath = RealPathUtil.getPath(A4Fit_Measurement.this, data.getData());
                    Log.e("selectedFile", "path:" + imageFilePath);

                    loadImageSource(imageFilePath);

                } else if (requestCode == Constants.CAPTURE_IMAGE_FROM_CAMERA) {

                    if (MyUtils.checkStringValue(imageFilePath)) {
                        Log.e("capturedImagePath", "path:" + imageFilePath);
                        loadImageSource(imageFilePath);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }

    private void loadImageSource(final String imageFilePath) {
        Glide.with(A4Fit_Measurement.this).load(imageFilePath).placeholder(R.drawable.ic_launcher_background).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                isImageLoaded = false;
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                isImageLoaded = true;
                if (isFrontImage) {
                    filePathFront = imageFilePath;
                    viewPager.setCurrentItem(1);
                    addBottomDots(1);
                } else {
                    filePathSide = imageFilePath;
                }
                return false;
            }
        }).into(isFrontImage ? frontImg : backImg);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_STORAGE_FILES_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToImageSelection();
                    return;
                } else {
                    if (A4Fit_Measurement.this == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(A4Fit_Measurement.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(A4Fit_Measurement.this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            case Constants.WRITE_STORAGE_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    intentToCameraApp();

                    return;
                } else {
                    if (A4Fit_Measurement.this == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(A4Fit_Measurement.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(A4Fit_Measurement.this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }

}
