package com.bestweb.custlr.activities.products.measurements;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.adapters.StandardSizeAdapter;
import com.bestweb.custlr.interfaces.DialogInterface;
import com.bestweb.custlr.interfaces.OnChooseSize;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.CustomDialog;
import com.bestweb.custlr.utils.MyUtils;

import java.util.ArrayList;

public class StandardSize extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ImageView imgBack;
    private TextView txtHeader;
    private LinearLayout linearLayoutNext;

    private ArrayList<String> arrayList = new ArrayList<>();
    String productId = "";
    String productName = "";
    String productPrice = "";
    String imgUrl = "";
    String size = "";
    String featuredImageUrl = "";
    String selectValue = "";
    String styleJsonValue = "";
    String measurementValue = "";


    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standard_size);
        context = StandardSize.this;

        init();

        loadDatas();

    }

    private void init() {

        imgBack = findViewById(R.id.imgBack);
        txtHeader = findViewById(R.id.txtHeader);
        linearLayoutNext = findViewById(R.id.as_linearSummaryButton);

        txtHeader.setText("SIZES");
        linearLayoutNext.setAlpha((float) 0.5);
        linearLayoutNext.setEnabled(false);

        recyclerView = findViewById(R.id.as_recycler);

        Bundle bundle = getIntent().getExtras();
        productId = bundle.getString(Constants.PRODUCT_ID);
        productName = bundle.getString(Constants.PRODUCT_NAME);
        productPrice = bundle.getString(Constants.PRODUCT_PRICE);
        imgUrl = bundle.getString(Constants.PRODUCT_IMAGE_URL);
        featuredImageUrl = bundle.getString(Constants.FEATURED_IMAGE_URL);
        selectValue = bundle.getString(Constants.SELECT);
        styleJsonValue = bundle.getString(Constants.STYLE_JSON_ARRAY);
        measurementValue = bundle.getString(Constants.MEASUREMENT_VALUES);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                MyUtils.openOverrideAnimation(false, StandardSize.this);
            }
        });

        linearLayoutNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToBodyMeasurement();
            }
        });

    }

    private void loadDatas() {

        arrayList.add("small");
        arrayList.add("medium");
        arrayList.add("large");
        arrayList.add("extra-large");


        final StandardSizeAdapter adapter = new StandardSizeAdapter(arrayList, context, new OnChooseSize() {
            @Override
            public void onClick(int pos) {

                linearLayoutNext.setAlpha((float) 1);
                linearLayoutNext.setEnabled(true);
                size = arrayList.get(pos);


            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
    }

    private void intentToBodyMeasurement() {
        if (MyUtils.checkStringValue(size)) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.PRODUCT_ID, productId);
            bundle.putString(Constants.PRODUCT_NAME, productName);
            bundle.putString(Constants.PRODUCT_PRICE, productPrice);
            bundle.putString(Constants.PRODUCT_IMAGE_URL, imgUrl);
            bundle.putString(Constants.FEATURED_IMAGE_URL, featuredImageUrl);
            bundle.putString(Constants.SIZE, size);
            bundle.putString(Constants.STYLE_JSON_ARRAY, styleJsonValue);
            bundle.putString(Constants.MEASUREMENT_VALUES, measurementValue);
            bundle.putString(Constants.SELECT, selectValue);

            Intent intent = new Intent(StandardSize.this, BodyMeasurement.class);
            intent.putExtras(bundle);
            startActivity(intent);
            MyUtils.openOverrideAnimation(true, StandardSize.this);
        } else {
            showAlertToUser(Constants.DIALOG_WARNING, "Size required");
        }
    }

    private void showAlertToUser(String statusCode, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(StandardSize.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {

            }

            @Override
            public void onDismissedClicked() {

            }
        });
        customDialog.showDialog();
    }

}
