package com.bestweb.custlr.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.products.ProductDetailActivity;
import com.bestweb.custlr.adapters.MyCartListAdapter;
import com.bestweb.custlr.interfaces.CartItemClickListener;
import com.bestweb.custlr.models.Cart;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgBack;
    private RelativeLayout cartRelative;
    //private TextView totalAmountTxt;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout backLinear, nextLinear;
    private ProgressBar progressBar;
    private RelativeLayout emptyViewRelative;

    private ProgressDialog progressDialog = null;
    private MyCartListAdapter cartListAdapter = null;
    ArrayList<Cart> cartArrayList = new ArrayList<>();

    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        context = CartActivity.this;

        init();
    }


    private void init() {

        imgBack = findViewById(R.id.ac_imgBack);
        cartRelative = findViewById(R.id.ac_cartRelative);
        //totalAmountTxt = findViewById(R.id.ac_totalAmountTxt);
        recyclerView = findViewById(R.id.ac_recyclerView);
        backLinear = findViewById(R.id.ac_backLinear);
        nextLinear = findViewById(R.id.ac_nextLinear);

        progressBar = findViewById(R.id.ac_progressBar);
        emptyViewRelative = findViewById(R.id.ac_emptyViewRelative);

        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        checkIfUserLoggedIn();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePage();
            }
        });

        backLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

    }

    private void checkIfUserLoggedIn() {
        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {


            getCartList(userId);


        } else {

            intentToLoginPage();
        }
    }


    private void getCartList(final String userId) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstanceWC().create(ApiInterface.class);
            Log.e("cart", ":" + "userId: " + userId);
            Call<String> call = apiService.getCartList(userId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    hideProgressBar();
                    try {
                        String jsonResponse = response.body();

                        Log.e("prdResponse", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase("ok")) {

                                cartArrayList.clear();

                                JSONArray jsonArray = jsonObject.optJSONArray("cart");

                                if (jsonArray != null && jsonArray.length() > 0) {
                                    Type listType = new TypeToken<ArrayList<Cart>>() {
                                    }.getType();
                                    Gson gson = new GsonBuilder().serializeNulls().create();
                                    cartArrayList = gson.fromJson(jsonArray.toString(), listType);

                                    adaptCartList(userId);
                                } else {
                                    showErrorCase();
                                }
                            } else {
                                showErrorCase();
                            }


                            //double totalPrice = 0.0;
                            //double totPrice = MyUtils.checkStringValue(price) ? (Double.parseDouble(price)) : 0.0;
                            //totalPrice = totalPrice + totPrice;
                            //totalPrice = Math.round(totalPrice);
                            //Log.e("totalPrice", ":" + totalPrice);


                        } else {
                            showErrorCase();
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                        showErrorCase();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    hideProgressBar();
                    showErrorCase();
                }
            });
        } catch (Exception e) {
            Log.e("exception44", ":" + e.getMessage());
            hideProgressBar();
            showErrorCase();
        }
    }


    private void adaptCartList(final String userId) {
        if (cartArrayList.size() > 0) {

            resetView();

            Log.e("size", ":" + cartArrayList.size());

            /*if (totalPrice != 0.0) {
                totalAmountTxt.setText("RM " + totalPrice); //TODO SHOW TOTAL CART PRICE
            }*/

            CartItemClickListener cartItemClickListener = new CartItemClickListener() {
                @Override
                public void onDeleteCartItem(int position, String productId) {

                    deleteCart(userId, productId, position);

                }

                @Override
                public void onCartItemClicked(int position) {

                    String productId = cartArrayList.get(position).getProductId();
                    String productName = cartArrayList.get(position).getProductName();
                    String productSalePrice = "" + cartArrayList.get(position).getPrice();

                    if (MyUtils.checkStringValue(productId)) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PRODUCT_ID, productId);
                        bundle.putString(Constants.PRODUCT_NAME, productName);
                        bundle.putString(Constants.PRODUCT_REGULAR_PRICE, "");
                        bundle.putString(Constants.PRODUCT_SALE_PRICE, productSalePrice);
                        bundle.putString(Constants.IS_FROM, Constants.IS_FROM_CART_PAGE);

                        Intent intent = new Intent(context, ProductDetailActivity.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, Constants.PRODUCTS);

                        MyUtils.openOverrideAnimation(true, (Activity) context);
                    }
                }

                @Override
                public void onEditClicked() {



                }
            };


            cartListAdapter = new MyCartListAdapter(context, cartArrayList, cartItemClickListener);
            recyclerView.setAdapter(cartListAdapter);


            nextLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(RequestParamUtils.user_id, userId);
                        jsonObject.put(RequestParamUtils.cartItems, getCartDataForAPI(cartArrayList));
                        jsonObject.put(RequestParamUtils.os, RequestParamUtils.android);
                        jsonObject.put(RequestParamUtils.deviceToken, ""); //TODO ADD FCM TOKEN HERE

                        Log.e("jsonInput", ":" + jsonObject.toString());

                        addProductToCheckOut(jsonObject.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            showErrorCase();
        }
    }


    private void deleteCart(final String userId, String productId, final int position) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Removing Product..");
            ApiInterface apiService = HttpRequest.getScalarsInstanceWC().create(ApiInterface.class);
            Call<String> call = apiService.deleteCartItem(productId, userId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        String jsonResponse = response.body();

                        Log.e("delete_item", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            JSONObject jsonObject = new JSONObject(jsonResponse);
                            String status = jsonObject.optString("status");
                            String message = jsonObject.optString("message");

                            if (MyUtils.checkStringValue(message)) {
                                MyUtils.ShowLongToast(context, message);
                            }

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase("1")) {
                                if (cartListAdapter != null) {
                                    cartArrayList.remove(position); //TODO CHECK ONCE
                                    cartListAdapter.notifyItemRemoved(position);

                                    if (cartArrayList.size() == 0) {
                                        //getCartList(userId);
                                        showErrorCase();
                                    }
                                }
                            }


                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e.getMessage());
            MyUtils.dismissProgressLoader(progressDialog);
        }

    }


    private void addProductToCheckOut(final String jsonInput) {

        Log.e("jsonInput", ":" + jsonInput);
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Checking out Product..");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.checkOutProduct(jsonInput);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        String jsonResponse = response.body();

                        Log.e("checkout", "resp:" + jsonResponse);

                        if (jsonResponse != null) {

                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (status.equalsIgnoreCase(Constants.SUCCESS)) {

                                String thankYouMainUrl = jsonObject.getString(RequestParamUtils.THANKYOU);
                                String thankYouUrl = jsonObject.getString(RequestParamUtils.THANKYOUEND);
                                String checkOutUrl = jsonObject.getString(RequestParamUtils.CHECKOUT_URL);
                                String homeUrl = jsonObject.getString(RequestParamUtils.HOME_URL);

                                Bundle bundle = new Bundle();
                                bundle.putString(RequestParamUtils.THANKYOU, thankYouMainUrl);
                                bundle.putString(RequestParamUtils.THANKYOUEND, thankYouUrl);
                                bundle.putString(RequestParamUtils.CHECKOUT_URL, checkOutUrl);
                                bundle.putString(RequestParamUtils.HOME_URL, homeUrl);
                                bundle.putString(RequestParamUtils.CHECKOUT_JSON_INPUT, jsonInput);


                                Intent intent = new Intent(context, CheckoutActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                MyUtils.openOverrideAnimation(true, CartActivity.this);


                            } else {

                                String message = "" + jsonObject.optString("message");

                                MyUtils.ShowLongToast(context, (MyUtils.checkStringValue(message) ? message : "Unable to checkout cart items"));

                            }


                        }
                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
        }


    }


    private void hideProgressBar() {
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }


    private void showErrorCase() {
        if (cartRelative.getVisibility() == View.VISIBLE) {
            cartRelative.setVisibility(View.GONE);
        }

        if (emptyViewRelative.getVisibility() == View.GONE) {
            emptyViewRelative.setVisibility(View.VISIBLE);
        }
    }

    private void resetView() {
        if (cartRelative.getVisibility() == View.GONE) {
            cartRelative.setVisibility(View.VISIBLE);
        }

        if (emptyViewRelative.getVisibility() == View.VISIBLE) {
            emptyViewRelative.setVisibility(View.GONE);
        }
    }


    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, CartActivity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.LOGIN_CODE) {
            if (resultCode == Constants.LOGIN_SUCCESS) {

                checkIfUserLoggedIn();

            } else if (resultCode == Constants.LOGIN_FAILED) {
                if (!MyUtils.checkIfUserLoggedIn(context)) {
                    closePage();
                }
            }
        }
    }


    private void closePage() {
        setResult(Constants.PRODUCTS);
        finish();
        MyUtils.openOverrideAnimation(false, CartActivity.this);
    }


    public JSONArray getCartDataForAPI(ArrayList<Cart> cartArrayList) {
        try {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < cartArrayList.size(); i++) {

                JSONObject object = new JSONObject();

                object.put(RequestParamUtils.PRODUCT_ID, "" + cartArrayList.get(i).getProductId());
                object.put(RequestParamUtils.quantity, "" + cartArrayList.get(i).getQuantity());

                jsonArray.put(object);
            }
            return jsonArray;
        } catch (Exception e) {
            Log.e("error", e.getMessage());
            return null;
        }
    }


}
